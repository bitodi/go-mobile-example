package library

import (
	"net/http"
	"io/ioutil"
	"encoding/json"
	"sync"
)

type Callback interface {
	OnError(message string)
	OnNextRepository(repository *Repository)
	OnFinish()
}

type githubErrorResponse struct {
	Message          string
	DocumentationUrl string
}

type githubRepository struct {
	Name        string
	HtmlUrl     string `json:"html_url"`
	Description string
	Language    string
}

type bitbucketResponse struct {
	Pagelen int
	Values  []bitbucketRepository
	Page    int
	Size    int
}

type bitbucketRepository struct {
	Name        string
	Description string
	Language    string
	Links struct {
		Html struct {
			Href string
		}
	}
}

type bitbucketErrorResponse struct {
	typeMessage string `json:"type"`
	Error struct {
		Message string
	}
}

type Repository struct {
	Name        string
	Description string
	HtmlUrl     string
	Provider    string
}

func FetchRepositories(name string, callback Callback) {

	var wg sync.WaitGroup
	wg.Add(2)

	go fetchFromGithub(name, callback, &wg)
	go fetchFromBitbucket(name, callback, &wg)
	go waitForFinish(callback, &wg)
}

func waitForFinish(callback Callback, wg *sync.WaitGroup) {
	// Wait for fetches to finish
	wg.Wait()

	callback.OnFinish()
}

func getUrl(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

func fetchFromGithub(name string, callback Callback, wg *sync.WaitGroup) {
	defer wg.Done()
	body, err := getUrl("https://api.github.com/users/" + name + "/repos")
	if err != nil {
		callback.OnError("Failed to get Github url: " + err.Error())
		return
	}

	// Unmarshal the json
	var repositories []githubRepository
	err = json.Unmarshal(body, &repositories)
	if err != nil {
		var errorResponse githubErrorResponse
		err = json.Unmarshal(body, &errorResponse)
		if err != nil {
			callback.OnError("Failed to parse Github json: " + err.Error())
			return
		}
		callback.OnError("Username " + name + " not found from Github")
		return
	}

	// Send repositories to callback
	for index := range repositories {
		repo := repositories[index]
		repository := Repository{repo.Name, repo.Description, repo.HtmlUrl, "Github"}
		callback.OnNextRepository(&repository)
	}
}

func fetchFromBitbucket(name string, callback Callback, wg *sync.WaitGroup) {
	defer wg.Done()
	body, err := getUrl("https://api.bitbucket.org/2.0/repositories/" + name)
	if err != nil {
		callback.OnError("Failed to get Bitbucket url: " + err.Error())
		return
	}

	// Unmarshal the json
	var response bitbucketResponse

	err = json.Unmarshal(body, &response)
	if err != nil {
		var errorResponse bitbucketErrorResponse
		err = json.Unmarshal(body, &errorResponse)
		if err != nil {
			callback.OnError("Failed to parse Bitbucket json: " + err.Error())
			return
		}
		callback.OnError("Username " + name + " not found from Bitbucket")
		return
	}

	// Send repositories to callback
	for index := range response.Values {
		repo := response.Values[index]
		repository := Repository{repo.Name, repo.Description, repo.Links.Html.Href, "Bitbucket"}
		callback.OnNextRepository(&repository)
	}
}
