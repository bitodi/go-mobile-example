package fi.bitodi.golibraryexample;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import library.Callback;
import library.Library;
import library.Repository;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private RepositoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // RecyclerView
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.main_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new RepositoryAdapter(new RepositoryAdapter.ClickListener() {
            @Override
            public void openUrl(String url) {
                final Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);

        handleIntent(getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the options menu from XML
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_main_search).getActionView();
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default

        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            final String username = intent.getStringExtra(SearchManager.QUERY);
            final List<Repository> repositories = new ArrayList<>();

            Library.fetchRepositories(username, new Callback() {
                @Override
                public void onNextRepository(Repository repository) {
                    Log.d(TAG, repository.getProvider() + " repository: " + repository.getName());
                    repositories.add(repository);
                }

                @Override
                public void onError(String s) {
                    Log.e(TAG, "Error while loading repositories for " + username + ": " + s);
                }

                @Override
                public void onFinish() {
                    Log.i(TAG, "User has " + repositories.size() + " repositories");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.updateRepositories(repositories);
                        }
                    });
                }
            });
        }
    }

    private static class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder> {

        interface ClickListener {
            void openUrl(String url);
        }

        private final List<Repository> repositories;
        private final ClickListener listener;

        RepositoryAdapter(ClickListener listener) {
            this.repositories = Collections.synchronizedList(new ArrayList<Repository>());
            this.listener = listener;
        }

        static class ViewHolder extends RecyclerView.ViewHolder {
            final TextView nameView;

            ViewHolder(View v) {
                super(v);
                nameView = (TextView) v.findViewById(R.id.adapter_name);
            }
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adapter_repositories, parent, false);

            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final Repository repository = repositories.get(position);
            holder.nameView.setText(repository.getName());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.openUrl(repository.getHtmlUrl());
                }
            });
        }

        @Override
        public int getItemCount() {
            return repositories.size();
        }

        void updateRepositories(List<Repository> repositories) {
            this.repositories.clear();
            this.repositories.addAll(repositories);
            this.notifyDataSetChanged();
        }
    }
}
