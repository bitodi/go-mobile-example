# Go mobile example

This repository contains an example Go mobile application. The app searches public repositories from GitHub and Bitbucket for the given name using a library written in Go.

### Repository content ###

* android - The android app source code.
* library - The library code written in Go

### Building the code ###

You need to have the Go mobile tools installed to build:

    $ go get golang.org/x/mobile/cmd/gomobile
    $ gomobile init

Once the tools are installed, the Android app can be built using Gradle. You can either use your local installation, the included gradlew script or Android Studio. The build creates an apk file which you can install to your Android device or an emulator.